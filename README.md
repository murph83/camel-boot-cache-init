Camel cache configuration example on Spring Boot
===================================

This application demonstrates configuring the Camel Cache component through application.properties, rather than directly in a route.

To use this approach:

* add the `camel-cache-starter` dependency in [pom.xml](pom.xml)
* in [application.properties](src/main/resources/application.properties), configure the cache component
    
    ```properties
    # cache component configuration
    camel.component.cache.configuration.cache-name=itemCache
    camel.component.cache.configuration.max-elements-in-memory=0
    camel.component.cache.configuration.eternal=true
    camel.component.cache.configuration.time-to-idle-seconds=0
    camel.component.cache.configuration.time-to-live-seconds=0
    ```
* in your [route](src/main/resources/camel/load-cache.xml), reference the cache
    
    ```xml
    <to uri="cache://itemCache"/>
    ```

N.B. that in this example we are setting max-elements-in-memory=0, which will allow the cache to grow without bound and may lead to memory problems if we don't manage the size of the cache manually. This is fine for a fixed reference set of known size, but dangerous when used with dynamic data.


Loading merged values into the cache
------------------------------------
This repository also contains an example ([load-cache.xml](src/main/resources/camel/load-cache.xml)) demonstrating how to merge multiple source rows into a single key in the cache. 

We simulate the retrieval of several rows from a database, each of which contains a key and a value. If there exists a cache entry with the same key, we wish to append the new value to the end of the existing value and re-set into the cache. 

This is easily accomplished by reusing the CamelCacheKey header, and merging the bodies with the [enrich](https://access.redhat.com/documentation/en-us/red_hat_jboss_fuse/6.3/html/apache_camel_development_guide/msgtran#MsgTran-Enricher) EIP and a custom [aggregation strategy](src/main/java/rh/cache/aggregation/AppendBodyEnrichmentStrategy.java)
```xml
<split>
    <simple>${body}</simple>

    <!-- We're going to use the same key to lookup and add. Only need to set it once -->
    <setHeader headerName="CamelCacheKey">
        <simple>${body[MY_KEY]}</simple>
    </setHeader>

    <!-- Move the query string into the body -->
    <setBody>
        <simple>${body[QS_QUERY_STR]}</simple>
    </setBody>

    <!-- Check for existing queries under the same key, and combine them with the "appendBody" strategy -->
    <enrich strategyRef="appendBody">
        <constant>cache://itemCache?operation=Get</constant>
    </enrich>

    <!-- Put the combined query into the cache -->
    <to uri="cache://itemCache?operation=Add"/>
</split>

```


Performing token replacement from cached values
-----------------------------------------------
Frequently, there may be a need to replace tokens in a message body with values from the cache. Fortunately, the camel-cache component includes a [CacheBasedTokenReplacer](https://github.com/apache/camel/blob/master/components/camel-cache/src/main/java/org/apache/camel/processor/cache/CacheBasedTokenReplacer.java) that makes this task trivial.
```java
@Bean
public CacheBasedTokenReplacer replaceBWithBranchCode(){
    return new CacheBasedTokenReplacer(cacheName, "BRANCH_CODE","#B#");
}
```

The [token-replacement.xml](src/main/resources/camel/token-replacement.xml) defines a camel route that looks up one of the queries loaded as above, then for each query, passes it through a set of `CacheBasedTokenReplacer`s. These replacers can be found defined in [Processors.java](src/main/java/rh/cache/Processors.java)
```xml
<split strategyRef="collectList">
    <tokenize token="~~~"/>

    <!-- Use processors to enrich queries. See Processors.java -->
    <process ref="replaceBWithBranchCode"/>
    <process ref="replaceCWithCustomerAccNo"/>

    <!-- Log queries after replacement -->
    <log message="${body}"/>
</split>
``` 

Using this example
------------------
```shell 
mvn spring-boot:run
``` 

To demonstrate the token replacement route, submit a GET request to [http://localhost:9080/q1](http://localhost:9080/q1)
