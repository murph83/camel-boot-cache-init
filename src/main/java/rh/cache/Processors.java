package rh.cache;

import org.apache.camel.processor.cache.CacheBasedTokenReplacer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Processors {

    @Value("${camel.component.cache.configuration.cache-name}")
    String cacheName;

    @Bean
    public CacheBasedTokenReplacer replaceBWithBranchCode(){
        return new CacheBasedTokenReplacer(cacheName, "BRANCH_CODE","#B#");
    }

    @Bean
    public CacheBasedTokenReplacer replaceCWithCustomerAccNo(){
        return new CacheBasedTokenReplacer(cacheName, "CUSTOMER_ACC_NO","#C#");
    }
}
