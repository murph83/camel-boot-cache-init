package rh.cache.helper;

import org.apache.camel.Handler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Queries {

    private static final List<Map<String, String>> QUERIES = new ArrayList<Map<String, String>>();
    static {
        // MY_KEY = q1
        Map<String, String> q1a = new HashMap<String, String>();
        q1a.put("MY_KEY", "q1");
        q1a.put("QS_QUERY_STR", "SELECT * from TABLE_A where branch = #B# and customer = #C#");
        Map<String, String> q1b = new HashMap<String, String>();
        q1b.put("MY_KEY", "q1");
        q1b.put("QS_QUERY_STR", "SELECT * from TABLE_B where branch = #B#");
        QUERIES.add(q1a);
        QUERIES.add(q1b);

        // MY_KEY = q2
        Map<String, String> q2 = new HashMap<String, String>();
        q2.put("MY_KEY", "q2");
        q2.put("QS_QUERY_STR", "SELECT * from TABLE_C where branch = #B#");
        QUERIES.add(q2);


        // MY_KEY = BRANCH_CODE
        Map<String, String> branchCode = new HashMap<String, String>();
        branchCode.put("MY_KEY", "BRANCH_CODE");
        branchCode.put("QS_QUERY_STR", "8675309");
        QUERIES.add(branchCode);


        // MY_KEY = CUSTOMER_ACC_NO
        Map<String, String> customerAccNo = new HashMap<String, String>();
        customerAccNo.put("MY_KEY", "CUSTOMER_ACC_NO");
        customerAccNo.put("QS_QUERY_STR", "00123456789");
        QUERIES.add(customerAccNo);
    }

    @Handler
    public List<Map<String, String>> loadQueries(){
        return QUERIES;
    }

}
