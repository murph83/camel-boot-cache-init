package rh.cache.helper;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.apache.camel.Handler;
import org.apache.camel.component.cache.DefaultCacheManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class CacheDump {

    Logger LOG = LoggerFactory.getLogger("cache-dump");

    @Value("${camel.component.cache.configuration.cache-name}")
    String cacheName;

    @Handler
    public void dumpCache() {
        Cache itemCache = new DefaultCacheManagerFactory().getInstance().getCache(cacheName);
        List keys = itemCache.getKeys();
        for (Map.Entry e : itemCache.getAll(keys).entrySet()) {
            LOG.info("{}: {}" , e.getKey(), e.getValue());
        }
    }
}
