package rh.cache.aggregation;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AbstractListAggregationStrategy;
import org.springframework.stereotype.Component;

@Component("collectList")
public class CollectListAggregationStrategy extends AbstractListAggregationStrategy<String>{

    public String getValue(Exchange exchange) {
        return exchange.getIn().getBody(String.class);
    }
}
