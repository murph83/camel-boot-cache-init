package rh.cache.aggregation;

import org.apache.camel.Exchange;
import org.apache.camel.component.cache.CacheConstants;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.springframework.stereotype.Component;

@Component("appendBody")
public class AppendBodyEnrichmentStrategy implements AggregationStrategy {

    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {

        // If the cache lookup found an existing query, add the current one after ~~~
        if (newExchange.getIn().getHeader(CacheConstants.CACHE_ELEMENT_WAS_FOUND) != null) {
            oldExchange.getIn().setBody(newExchange.getIn().getBody(String.class) + "~~~" + oldExchange.getIn().getBody(String.class));
        }

        return oldExchange;
    }

}
